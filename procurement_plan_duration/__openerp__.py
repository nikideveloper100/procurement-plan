{
    'name' : 'Procurement Plan Duration',
    'version' : '1.0',
    'author' : 'Niks',
    'category' : 'Procurement Plan Duration',
    'description': """

    """,
    'website': 'Nothing',
    'images' : [],
    'depends' : ['procurement',
                'project',
                'purchase',
                'sale_stock',
                'mail',
                'procurement_manager'],
    'data': [
        "procurement_plan_duration_view.xml"
    ],
    'installable': True,
    'auto_install': False,
}