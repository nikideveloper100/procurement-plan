from openerp import models, fields, api, exceptions, _
from datetime import datetime, date, timedelta
from dateutil.relativedelta import relativedelta
import time
from openerp.exceptions import ValidationError

class procurement_plan(models.Model):
    _inherit = 'procurement.plan'
    
    duration_time = fields.Char(string='Duration')
    from_date = fields.Date('Start Date')
    to_date = fields.Date('End Date')
   
    def create(self, cr, uid, vals, context=None):
            result = super(procurement_plan, self).create(cr, uid, vals, context=context)
            for record in self.browse(cr, uid, [result]):
                d1 = datetime.strptime(str(record.from_date), '%Y-%m-%d')
                d2 = datetime.strptime(str(record.to_date), '%Y-%m-%d')
                if d1.strftime("%B")==d2.strftime("%B"):
                    print "Cool"
                else:
                    raise ValidationError("You have to enter same month of date")
                procurement_ids=self.search(cr, uid, [])
                del procurement_ids[-1]
                for procurement in self.browse(cr, uid, procurement_ids):
                    dd1 = datetime.strptime(str(procurement.from_date), '%Y-%m-%d')
                    dd2 = datetime.strptime(str(procurement.to_date), '%Y-%m-%d')
                    if procurement.warehouse_id==record.warehouse_id:
                        if d1.strftime("%B")==dd1.strftime("%B"):
                            raise ValidationError("Same month plan you can not create")
                        else:
                            print "cool"
 
            return result
